package com.company;

public class Main {

    public static void main(String[] args) {
      /*
       Example:
        In this project we put bad code under comments to be able to compare!
       */

        runPlayer();

       /*
        runPlayer1();
        runPlayer2();
        */


    }
    /*
    public static void runPlayer1() {
        run();
        if (tired()) {
            relax();
        } else if (obstacle()) {
            jump();
        }
        if (finished()) {
            System.out.println("Player 1 finished! ");
        }
    }

    public static void runPlayer2() {
        run();
        if (tired()) {
            relax();
        } else if (obstacle()) {
            jump();
        }
        if (finished()) {
            System.out.println("Player 2 finished! ");
        }
    }
*/
    public static void runPlayer() {
        run();
        if (tired()) {
            relax();
        } else if (obstacle()) {
            jump();
        }
        if (player1Finished()) {
            System.out.println("Player 1 finished! ");
        } else if (player2Finished()) {
            System.out.println("Player 2 finished! ");
        }
    }

    public static void run() {
        System.out.println("Run");
    }

    public static void jump() {
        System.out.println("Jump");
    }

    public static void relax() {
        System.out.println("Relax");
    }

    public static boolean tired() {
        return true;
    }

    public static boolean obstacle() {
        return true;
    }

    public static boolean finished() {
        return true;
    }

    public static boolean player1Finished() {
        return true;
    }

    public static boolean player2Finished() {
        return true;
    }
}
